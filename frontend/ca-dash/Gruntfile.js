module.exports = function(grunt) {
  grunt.initConfig({
    //env: grunt.file.readJSON('.env'),
    //YOU MUST CHANGE THIS FOR DEV AND PRODUC (BUCKET)
    s3: {
      options: {
        key: 'AKIAJLJXMT73IPQAYSKA',
        secret: '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',
        //bucket: 'campaignanalytics2',
        bucket: 'campaignanalyticsdev',
        access: 'public-read',
        headers: {
          "Cache-Control": "max-age=630720000, public",
          "Expires": new Date(Date.now() + 630720000).toUTCString()
        }
      },
      dev: {
        upload: [
          {
            src: 'dist/assets/**/*',
            dest: 'campaign-analytics/assets/',
            rel: 'dist/assets',
            options: { verify: true }
          }
        ]
      }
    },
    redis: {
      options: {
        manifestKey: 'releases',
        manifestSize: 10,
        //host: 'tarpon.redistogo.com',
        host: 'lab.redistogo.com',
        //port: '10460',
        port: '10265',
        connectionOptions: {
          //auth_pass: 'ec65400df85fc5c0ef0d32c8f7d7c3ad'
          auth_pass: 'c3a2ce498be46e1476c3cab4691c00cc'
        }
      },
      canary: {
        options: {
          prefix: '<%= gitinfo.local.branch.current.shortSHA %>:',
          currentDeployKey: '<%= gitinfo.local.branch.current.shortSHA %>',
        },
        files: {
          src: ["dist/index.html"]
        }
      },
      release: {
        options: {
          prefix: 'release:'
        },
        files: {
          src: ["dist/index.html"]
        }
      }
    },
  });
  grunt.loadNpmTasks('grunt-gitinfo');
  grunt.loadNpmTasks('grunt-s3');
  grunt.loadNpmTasks('grunt-redis');
  grunt.registerTask('release', ['gitinfo', 'redis:release']);
  grunt.registerTask('canary', ['gitinfo', 'redis:canary']);
  grunt.registerTask('publish-release', ['default', 'release']);
  return grunt.registerTask('default', ['gitinfo', 's3:dev', 'canary']);
};