/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

var env = EmberApp.env()|| 'development';

var fingerprintOptions = {
  enabled: true,
  extensions: ['js', 'css', 'png', 'jpg', 'gif']
};


switch (env) {
  case 'development':
    fingerprintOptions.prepend = 'http://analytics.dev:5000/';
  break;
  case 'production':
    fingerprintOptions.prepend = 'https://d2tmpc990nsj46.cloudfront.net/campaign-analytics/';
  break;
  case 'dev':
    fingerprintOptions.prepend = 'https://d227aozqvivtc7.cloudfront.net/campaign-analytics/';
  break;
}

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    // Add options here
    fingerprint: fingerprintOptions,

    minifyCSS: {
      enabled: true,
      options: {}
    },

    emberCLIDeploy: {
      runOnPostBuild: (env === 'development') ? 'development-postbuild' : false,
      configFile: 'config/deploy.js',
      shouldActivate: true
    },
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  app.import('bower_components/bootstrap/dist/css/bootstrap.css');
  app.import('bower_components/bootstrap/dist/css/bootstrap.css.map', { destDir: 'assets' });
  app.import('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.eot', { destDir: 'fonts' });
  app.import('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.svg', { destDir: 'fonts' });
  app.import('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.ttf', { destDir: 'fonts' });
  app.import('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff', { destDir: 'fonts' });
  app.import('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff2', { destDir: 'fonts' });

  app.import('bower_components/moment/moment.js');
  
  return app.toTree();
};
