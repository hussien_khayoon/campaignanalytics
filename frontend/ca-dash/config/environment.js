/* jshint node: true */

var contentSecurityPolicy = {
  'default-src': "'none'",
  'script-src': "'self'",
  'font-src': "'self'",
  'connect-src': "'self' localhost:* analytics.dev:* sleepy-mesa-1645.herokuapp.com:* campaign-analytics.herokuapp.com:*",
  'img-src': "'self'",
  'style-src': "'self' 'unsafe-inline'",
  'media-src': "'self'"
};

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'ca-dash',
    environment: environment,
    baseURL: '/',
    locationType: 'auto',
    apiHost: 'http://analytics.dev:3000',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },
    contentSecurityPolicy: contentSecurityPolicy,
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV.contentSecurityPolicy = contentSecurityPolicy;
    ENV.contentSecurityPolicy['script-src'] = "'self' 'unsafe-eval'";
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';
    ENV.apiHost = '';
    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
    ENV.apiHost = 'https://sleepy-mesa-1645.herokuapp.com'
  }
  else if (environment === 'dev'){
    ENV.apiHost = 'https://campaign-analytics.herokuapp.com'
  }

  return ENV;
};
