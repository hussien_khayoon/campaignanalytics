export function formatDate(params) {
  let unformattedDate = params[0];
  let date            = new Date(unformattedDate);
  return moment(date).format('M/D/YYYY');
}

export default Ember.Helper.helper(formatDate);