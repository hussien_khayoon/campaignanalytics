import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'span',
  
  actions: {
    saveText(text) {
      this.sendAction('saveText', text);
    },
  }
});
