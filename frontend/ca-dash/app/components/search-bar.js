import Ember from 'ember';

export default Ember.Component.extend({
  searchText: '',
  actions: {
    filter() {
      this.sendAction('filter', this.get('searchText'));
    },
  }
});
