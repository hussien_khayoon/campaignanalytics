import Ember from 'ember';

export default Ember.Component.extend({

  submit(event){
      event.preventDefault();
      this.sendAction('save');   
  },
  actions: {
    cancel() {
      this.sendAction('cancel');
    },
  }
});
