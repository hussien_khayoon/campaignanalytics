import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'tr',
  edit: false,

  mouseEnter: function(){
    this.set('edit', true);      
  },
  
  mouseLeave: function(){
    this.set('edit', false);
  },

  actions: {
    saveText(text) {
      console.log(this.get('campaign').id);
      this.sendAction('saveText', this.get('campaign').id, text)
    },
  }
});
