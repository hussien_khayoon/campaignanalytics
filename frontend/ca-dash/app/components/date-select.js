import Ember from 'ember';

export default Ember.Component.extend({
  ranges: ['Today', 'Yesterday', 'Last Week', 'Last Month', 'Custom'],
  selectedRange: 'Today',
  showDatepicker: false,
  actions: {

    changeRange(selectedRange)
    {
      this.set('selectedRange', selectedRange);

      if(selectedRange === 'Custom')
      {
        this.set('showDatepicker', true);
      }
      else
      {
        this.set('showDatepicker', false);
        
        this.sendAction('changeRange', selectedRange);
      }
    },

    // For datepicker when it's enabled in 'Custom' range
    startDateValue(startDate){
      this.sendAction('startDateValue', startDate);
    },

    endDateValue(endDate){
      this.sendAction('endDateValue', endDate);
    }
  }
});