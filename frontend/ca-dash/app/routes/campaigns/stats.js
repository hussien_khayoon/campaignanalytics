import Ember from 'ember';

export default Ember.Route.extend({
  ajax: Ember.inject.service(),

  setupController: function (controller,model) {
      // Get a single book
      Ember.$.getJSON('/shopify/campaign-orders?campaign='+model.id, function (data) {
        controller.set('model',data);
      });
  },
});