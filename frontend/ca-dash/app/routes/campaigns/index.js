import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return Ember.RSVP.hash({
      shops:      this.store.findAll('shop'),
      campaigns:  this.store.findAll('campaign')
    });
  },

  actions: {
    saveText(campaignID, text) {
      //let campaign = this.store.findRecord('campaign', campaignID);
      this.store.findRecord('campaign', campaignID).then(function(campaign) {


        campaign.set('spend', text);

        campaign.save(); // => PATCH to '/posts/1'
      });
      //console.log(campaign);
      //campaign.save();
    },

    changeRange(selectedRange)
    {
      var new_campaign = this.get('store').query('campaign', { range: selectedRange.toLowerCase() });
      console.log(new_campaign);
    },

    startDateValue(startDate){
      console.log(startDate);
    },

    endDateValue(){

    }
  }
});