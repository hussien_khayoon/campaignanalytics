import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.createRecord('campaign');
  },

  actions: {
    save() {
      var new_campaign = this.modelFor('campaigns.new');
      new_campaign.save().then((campaign) => {
        this.transitionTo('campaigns.show', campaign.id);
      });

    },
    cancel() {
      var new_campaign = this.modelFor('campaigns.new');
      new_campaign.destroyRecord()
      this.transitionTo('campaigns.index');
    }
  }

});
