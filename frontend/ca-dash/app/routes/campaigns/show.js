import Ember from 'ember';

export default Ember.Route.extend({
  model: function(params) {
    return Ember.RSVP.hash({
      shops:     this.store.findAll('shop'),
      campaign:  this.store.findRecord('campaign', params.campaign_id)
    });
  }
});
