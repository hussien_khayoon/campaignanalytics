import Ember from 'ember';

export default Ember.Route.extend({
  actions: {
    save() {
      var new_campaign = this.modelFor('campaigns.edit');
      new_campaign.save().then((campaign) => {
        this.transitionTo('campaigns.show', campaign.id);
      });
    },
    delete() {
      var new_campaign = this.modelFor('campaigns.edit');
      new_campaign.destroyRecord().then((campaign) => {
        this.transitionTo('campaigns.index');
      });
    },
    cancel() {
      this.transitionTo('campaigns.index');
    }
  }
});