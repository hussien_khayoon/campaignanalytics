import Ember from 'ember';
const { Controller, computed} = Ember;

export default Controller.extend({
  searchTerm:  '',

  filteredCampaigns: Ember.computed('model.campaigns.@each.name',
  'searchTerm', function() {
    return this.get('model.campaigns').filter((campaign) => {
      const searchTerm = this.get('searchTerm').toLowerCase();
      return campaign.get('name').toLowerCase().indexOf(searchTerm) !== -1;
    });
  }),

  actions: {
    filter(searchText) {
      this.set('searchTerm', searchText);
    },
  }

});