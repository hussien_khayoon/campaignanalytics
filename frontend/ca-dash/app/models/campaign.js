import DS from 'ember-data';

export default DS.Model.extend({
  name:       DS.attr('string'),
  active:     DS.attr('boolean'),
  views:      DS.attr('number'),
  sales:      DS.attr('number'),
  revenue:    DS.attr('number'), 
  spend:      DS.attr('number'),
  created_at: DS.attr('date'),  
  param:      DS.attr('string'), 
  shop:       DS.belongsTo('shop'),

  date: Ember.computed('created_at', function() {
    let date        = new Date(this.get('created_at'));
    return moment(date).format('M/D/YYYY');
  }),

  status: Ember.computed('active', function() {
    let status = this.get('active') ? 'Active' : 'Inactive';
    return status;
  }),

  adspendPerOrder: Ember.computed('sales', 'spend', function(){
    return this.get('sales') === 0 ? 0 : parseInt(this.get('spend')/this.get('sales'));
  }),

  adVsSpend: Ember.computed('spend', 'revenue', function(){
    let adVsSpend;
    if(this.get('revenue') === 0)
    {
      adVsSpend = 0;
    }
    else
    {
      adVsSpend = parseInt((this.get('spend') / this.get('revenue'))*100);
    }
    return adVsSpend;
  })
});
