import DS from 'ember-data';

export default DS.Model.extend({
  shop_url:  DS.attr('string'),
  shop_name: DS.attr('string'),
  campaigns: DS.hasMany('campaign')
});
