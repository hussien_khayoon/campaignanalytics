import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('campaigns', { path: '/dashboard' }, function () {
    this.route('new', {path: '/new'});
    this.route('show', {path: '/campaign/:campaign_id'});
    this.route('edit', {path: '/campaign/edit/:campaign_id'});
    this.route('stats', {path: '/campaign/stats/:campaign_id'});
  });
});

export default Router;
