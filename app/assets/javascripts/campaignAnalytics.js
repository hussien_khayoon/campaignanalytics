function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}

var ref = getURLParameter('ref');
var onCheckoutPageIdx = window.location.href.indexOf("//checkout.shopify.com/");


console.log("your ref is: " + ref);

  
function do_ajax_post_without_jquery( url, body, success_callback, fail_callback) 
{
    var xmlhttp = new XMLHttpRequest();
  
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 ) {  //XMLHttpRequest.DONE 
          console.log("xmlhttpstatus is " + xmlhttp.status);
           if(xmlhttp.status == 200){
        success_callback(document, xmlhttp)
           }
           else  {
        fail_callback(document, xmlhttp)
           }
        } 
    }
  // "GET", "URL", "async (true/false)"
  xmlhttp.open('POST', url, true);
  
  xmlhttp.withCredentials = true;
  xmlhttp.setRequestHeader("Content-type", "application/json");
  
  xmlhttp.send(body)
}

if (ref) {

    
  
  function findFirstMatchingNodeByAttribute(allElements, attribute, contains_text)
  {
    for (var i = 0, n = allElements.length; i < n; i++)
    {
      src_attribute = allElements[i].getAttribute('src'); 
  
      if (src_attribute.indexOf(contains_text) >= 0)
      {
        return allElements[i];
      }
    }
    return null;
  }
  
  function getUrlParamForUrl(url, name) {
      var match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
      return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
  }
  
  
  function get_shop_name_from_script_call() {
  
    var script_node = findFirstMatchingNodeByAttribute( document.getElementsByTagName('script'), 
                              'src', 
                              'campaign_analytics.js');
    
    return getUrlParamForUrl(script_node.getAttribute('src'), 'shop');
  
  }
  
  function get_legacy_json_string() {
    var paramObj = {};
  
    var substr = window.location.href.substring(onCheckoutPageIdx+"//checkout.shopify.com/".length).split("/");
    var token = substr[2].split("?")[0];
  
    paramObj['shopDomain'] = get_shop_name_from_script_call();
    paramObj['token'] = token;    
    
    return JSON.stringify(paramObj);
  }

  
  function get_json_string(node) {
  
    var paramObj = {};
    
    shop = node.getAttribute('data-shop');
    
    if (shop == null) {
      shop = get_shop_name_from_script_call();
    }
    
    paramObj['shop_url'] = shop;
    paramObj['order_id'] = node.getAttribute('data-order-id');
    paramObj['customer_email'] = node.getAttribute('data-customer-email');
    paramObj['customer_name'] = node.getAttribute('data-customer-name');
    
    return JSON.stringify(paramObj);
  }

  var jsonArr = []; // Initialize here

  if (ref){
    jsonArr.push({
      //product_id:     product_id,
      ref     :       ref,
      shop    :       Shopify.shop
    });    
  } else{

  }

  var json_body = JSON.stringify(jsonArr);
  console.log(json_body);


  var json_body = JSON.stringify(jsonArr);
  console.log(json_body);
  
  do_ajax_post_without_jquery('https://sleepy-mesa-1645.herokuapp.com/shopify/create_cookie',
      json_body, 
      function(doc, xmlhttp) 
      {         
        console.log("SUCCESS");

      },
      function(doc, xmlhttp) { 
        console.log("FAIL");       
  });
}

function read_cookie(key)
{
    var result;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
}

if (onCheckoutPageIdx!=-1) {


  var jsonArr = []; // Initialize here

  jsonArr.push({
    shopify:                  Shopify,
    campaign_analytics_ref:   read_cookie('campaign_analytics_ref')
  });    

  var json_body = JSON.stringify(jsonArr);
  console.log(json_body);

  do_ajax_post_without_jquery('https://sleepy-mesa-1645.herokuapp.com/shopify/order_confirmation',
      json_body, 
      function(doc, xmlhttp) 
      {         
        console.log("SUCCESS");

      },
      function(doc, xmlhttp) { 
        console.log("FAIL");       
  });
}
