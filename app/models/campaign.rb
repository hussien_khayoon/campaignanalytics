class Campaign < ActiveRecord::Base
  belongs_to :shop
  has_many :referral_records
  has_many :daily_stats
end
