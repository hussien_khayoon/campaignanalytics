class Shop < ActiveRecord::Base
  has_many :campaigns
  has_many :referral_records
end
