class ShopSerializer < ActiveModel::Serializer
  attributes  :id, :shop_url, :shop_name

  embed :ids, :include => true
end
