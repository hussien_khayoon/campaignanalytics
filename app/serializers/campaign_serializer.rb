class CampaignSerializer < ActiveModel::Serializer
  attributes  :id, :name, :active, :views, :sales, :revenue, :spend, :param, :created_at, :shop_id

  embed :ids, :include => true
end
