require 'json'

class ShopsController < ApplicationController
  skip_before_filter :verify_authenticity_token, :only => [:create_cookie, :record_stats, :app_uninstall, :order_confirmation_event]

  if Rails.env.production?
    API_KEY = ENV['API_KEY']
    SHARED_SECRET = ENV['SHARED_SECRET']
  else
    API_KEY = '510db030e8d2ee6b9df0d053a23dfecb'
    SHARED_SECRET = 'fff83095c6e94c64ffa42221481ef940'
  end

  def index
  end

  def new
    @shop = Shop.new
  end

  def create 
    @shop = Shop.new(shop_params)
    shop_url =  shop_params[:shop_url]

    
    ShopifyAPI::Session.setup({:api_key => API_KEY, :secret => SHARED_SECRET})

    # Start the shopify by specifying a specific shopify store to get authorized
    shopify_session = ShopifyAPI::Session.new(shop_url)

    # Define the level of access you want in your scope.
    # Then call upon the permission url to initiate OAuth
    scope = ["read_orders", "write_script_tags"]

    redirect_uri = request.base_url + '/shopify/callback'

    permission_url = shopify_session.create_permission_url(scope, redirect_uri)

    

    redirect_to permission_url
  end

  def finalize_install
    shopify_session = ShopifyAPI::Session.new(params[:shop])
    token = shopify_session.request_token(params)

    shop = Shop.find_by(shop_url: params[:shop])

    if shop
      # ignore this python stuff request.session['shop'] = params['shop']

      shop.is_installed = true 
      shop.access_token = token
      shop.save


    else # if shop doesn't exist in our db
      shopify_session = ShopifyAPI::Session.new(params[:shop], token)
      ShopifyAPI::Base.activate_session(shopify_session)
      shopify_shop = ShopifyAPI::Shop.current
      shop = Shop.new
      shop.is_installed = true
      shop.shop_url = params[:shop]
      shop.access_token = token
      shop.shopify_shop_id = shopify_shop.id
      shop.save

      # The script tag needs to be created in both cases in case of an uninstall where it is removed
      js_url = ENV['JS_URL']
      script_tag = ShopifyAPI::ScriptTag.create(:event => 'onload', :src => js_url)

      # TODO: clearly these links have to match the url of the app, there are not needed yet however
      # register a webhook for created orders and uninstall
      
      ShopifyAPI::Webhook.create(:topic => 'orders/create', 
        :address => 'https://campaign-analytics.herokuapp.com/shopify/webhook/orders/create', :format => 'json')
      
      ShopifyAPI::Webhook.create(:topic => 'app/uninstalled', 
        :address => 'https://campaign-analytics.herokuapp.com/shopify/webhook/app/uninstalled', :format => 'json')
      
    end

    session[:shop_url] = params[:shop]

    redirect_to '/dashboard'
  end

  def login
    logger.info("login() called")
    
    #TODO check for cookie here 
    
    shop_url        = params[:shop]
    
    if not shop_url
        return render :status => :forbidden, :text => "Forbidden fruit"
    end
  
    if  params[:signature] == ''
        return render :status => :forbidden, :text => "Forbidden fruit"
    end

    shop = Shop.find_by(shop_url: shop_url)

    ShopifyAPI::Session.setup({:api_key => API_KEY, :secret => SHARED_SECRET})
    verified        = ShopifyAPI::Session.validate_signature(params)

    
    if not verified
        logger.debug('not verified')
        return render :text => "not verified, please login"
    else
        # TODO creat login cookie
        # shop.login_cookie   = uuid.uuid4()   
        # shop.save()
        session[:shop_url] = shop_url
    end
  
    redirect_to '/dashboard'
  end

  def app_uninstall
    logger.info('app_uninstall called')

    logger.debug(params[:myshopify_domain])

    shop_url = params[:myshopify_domain]

    if not shop_url 
      logger.error('myshopify_domain is not returned by webhook app uninstalled')
    end

    shop = Shop.find_by(shop_url: shop_url)

    if not shop
      logger.error('trying to uninstall a shop that we do not recognize')
    end

    shop.is_installed = false 
    shop.save

    render :status => 200, :text => 'app uninstalled successfully'
  end


  def orders
    shop = Shop.find_by(shop_url: 'hussiens-test-shop.myshopify.com')
    session = ShopifyAPI::Session.new(shop.shop_url, shop.access_token)
    ShopifyAPI::Base.activate_session(session)
    shopify_shop = ShopifyAPI::Shop.current
 
    ids = '2972207814,2972204358,2972186502'
    # Get a specific product
    orders = ShopifyAPI::Order.where(:ids => ids, :status => 'any')

    render :json => orders
  end


  # In this method we want to create a cookie to keep track of link views and sales.
  # We check if cookie exists, then record a view for the campaign. If not create a new cookie.
  def create_cookie    
    logger.info('detected ref')

    json = params[:_json]

    if json.nil?
      # TODO specify error message
      logger.error("no ref was passed to this method")
      return render(json: error, status: error.status)
    end

    ref       = json.first['ref']
    shop_url  = json.first['shop']

    # If we've already added this ref to cookies, then don't overwrite cookie
    if cookies[:campaign_analytics_ref] and cookies[:campaign_analytics_ref] == ref

    else # create a new cookie or overwrite the last one
      cookies[:campaign_analytics_ref] = ref
      logger.debug(cookies[:campaign_analytics_ref])
    end

    #increment view on db for campain
    shop     = Shop.find_by(shop_url: shop_url)
    campaign = Campaign.find_by(param: ref, shop_id: shop.id)

    if campaign
      dailyStat = DailyStat.where("created_at >= ? and campaign_id = ?", 
        Time.zone.now.beginning_of_day, campaign.id).first

      if dailyStat
        dailyStat.views       = dailyStat.views + 1
        dailyStat.save
      else
        dailyStat = DailyStat.new
        dailyStat.views       = dailyStat.views + 1
        dailyStat.campaign_id = campaign.id
        dailyStat.save
      end 
    else
      logger.error('the ref' + ref.to_s + ' does not exist in db')
    end

    return render :status => 200, :text => 'cookie created'
  end


  def record_stats
    logger.info('sales occurred')

    #first let let's see if the sale was caused by us
    unless cookies[:campaign_analytics_ref]
      logger.info('not our sale')
      return render :status => 200, :text => 'Not our sale, skip'
    else
      logger.info('our cookie was detected')
     
      total     = params[:_json][0][:shopify][:checkout][:total_price]
      shop_url  = params[:_json][0][:shopify][:shop]

      ref       = cookies[:campaign_analytics_ref]

      # This is wrong and it needs to be fixed, should also be by shop
      shop      = Shop.find_by(shop_url: shop_url)
      campaign  = Campaign.find_by(param: ref, shop_id: shop.id)

      shopify_order_id = params[:_json][0][:shopify][:checkout][:order_id]

      if campaign

        referralRecord = ReferralRecord.find_by(shopify_order_id: shopify_order_id)

        if referralRecord
          logger.info('we already processed this order ' + shopify_order_id.to_s)
        else
          referralRecord                   = ReferralRecord.new
          referralRecord.ref               = ref   
          referralRecord.shopify_order_id  = shopify_order_id
          referralRecord.shop_id           = campaign.shop_id
          referralRecord.campaign_id       = campaign.id
          referralRecord.save

          # Here we need to update the daily stats table
          # 1) We get the latest daily stats record
          # 2) if created today, update stats
          # 3) else, create a new daily stats record and add stats
          #campaign  = Campaign.find_by(param: ref, shop_id: shop.id)
          dailyStat = DailyStat.where("created_at >= ? and campaign_id = ?", 
            Time.zone.now.beginning_of_day, campaign.id).first          
          if dailyStat
            dailyStat.sales   = dailyStat.sales + 1
            dailyStat.revenue = dailyStat.revenue + total.to_f
            dailyStat.save
          else
            dailyStat = DailyStat.new
            dailyStat.views       = dailyStat.views + 1
            dailyStat.sales       = dailyStat.sales + 1
            dailyStat.revenue     = dailyStat.revenue + total.to_f
            dailyStat.campaign_id = campaign.id
            dailyStat.save
          end            
        end
      
      else
        logger.error('Could not find the ref #{ref}')
      end
      return render :status => 200, :text => 'Sale was ours'
    end
  end

  # This is the method that hits when orders/create webhook hits
  def order_confirmation_event
    #WebhookWorker.perform_async()
    logger.info('order confirmation webhook fired')

    total     = params[:total_price]

    # First try to find the order in the db referralRecords
    shopify_order_id = params[:id]
    referralRecord = ReferralRecord.find_by(shopify_order_id: shopify_order_id)

    # if ref record already found, then we don't need to be here
    if referralRecord
      logger.info('we already processed this order ' + shopify_order_id.to_s)
      return render :status => 200, :text => 'Order already processed'
    else
      ref = params[:landing_site_ref]
      if not ref
        logger.info('We can\'t find the ref for this order, skipt it')
      else
        order_status_url = params[:order_status_url]
        if order_status_url

          # extract shopify_shop_id from order_status_url
          str1_markerstring = 'checkout.shopify.com/'
          str2_markerstring = '/checkouts'
          shopify_shop_id = order_status_url[/#{str1_markerstring}(.*?)#{str2_markerstring}/m, 1]

          # conver it to int so it can be used to retrieve it in db
          shopify_shop_id = shopify_shop_id.to_i

          shop      = Shop.find_by(shopify_shop_id: shopify_shop_id)
          campaign  = Campaign.find_by(param: ref, shop_id: shop.id)

          referralRecord                   = ReferralRecord.new
          referralRecord.ref               = ref  
          referralRecord.shopify_order_id  = shopify_order_id
          referralRecord.shop_id           = campaign.shop_id
          referralRecord.campaign_id       = campaign.id
          referralRecord.save

          # Retrieve the campaign related to the ref and inrease
          # sales by one
          campaign.sales   = campaign.sales + 1
          campaign.revenue = campaign.revenue + total.to_f
          campaign.save
        end
      end
    end 
    return render :status => 200, :text => 'OK'
  end

  def options_orderconf
    logger.info('option for order confirmation launched')

    origin = request.headers["HTTP_ORIGIN"]
    headers['Access-Control-Allow-Origin'] = origin
    headers['Access-Control-Allow-Methods'] = 'POST, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'X-ACCESS_TOKEN, Access-Control-Allow-Origin, Authorization, Origin, x-requested-with, Content-Type, Content-Range, Content-Disposition, Content-Description'
    headers['Access-Control-Allow-Credentials'] = 'true'
    return render :status => 200, :text => '', :content_type => 'text/plain'

  end

  def get_orders_for_campaign
    logger.info('getting orders for campaign')

    shop_url  = session[:shop_url]
    shop      = Shop.find_by(shop_url: shop_url)

    puts(params)
    campaign_id = params[:campaign]
    puts(campaign_id)

    referralRecords = ReferralRecord.where(campaign_id: campaign_id, shop_id: shop.id)

    ids     = ''
    orders  = []

    if referralRecords
      for referralRecord in referralRecords
        logger.info(referralRecord.shopify_order_id)
        ids << referralRecord.shopify_order_id + ','
      end

      logger.info(ids)

      if ids != '' and ids != ','
        session = ShopifyAPI::Session.new(shop.shop_url, shop.access_token)
        ShopifyAPI::Base.activate_session(session)
        shopify_shop = ShopifyAPI::Shop.current

        # Get a specific product
        orders = ShopifyAPI::Order.where(:ids => ids, :status => 'any')
      end
    end

    
    render :json => orders    
  end

 private

  def shop_params
    params.require(:shop).permit(:shop_url)# here go you parameters for an shop
  end

  def install_shop(shop_url)
    # Setup the Shopif API session using api key and secret
    ShopifyAPI::Session.setup({:api_key => API_KEY, :secret => SHARED_SECRET})

    # Start the shopify by specifying a specific shopify store to get authorized
    session = ShopifyAPI::Session.new(shop_url)

    # Define the level of access you want in your scope.
    # Then call upon the permission url to initiate OAuth
    scope = ["write_products"]
    redirect_uri = 'http://analytics.dev:3000/shopify/callback'

    permission_url = session.create_permission_url(scope,redirect_uri)

  end

end
