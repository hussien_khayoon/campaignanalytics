class HomeController < ApplicationController

  def index
    # logger.info('the deploy key is now')
    # logger.info(index_html)
    render text: add_keys_to_index(index_html)
  end

  private

  def index_html 
    if Rails.env.development?
      redis.get "#{deploy_key}"
    else
      redis.get "#{deploy_key}:index.html"
    end
  end

  # By default serve release, if canary is specified then the latest
  # known release, otherwise the requested version.
  def deploy_key
    if Rails.env.development?
      'ca-dash:__development__'
    else
      params[:version] ||= 'release'
      case params[:version]
      when 'release' then 'release'
      when 'canary'  then  redis.lindex('releases', 0)
      else
        params[:version]
      end
    end
  end

  def redis
    if Rails.env.development?
      redis = Redis.new()
    else
      logger.info(ENV['REDISTOGO_URL'])
      redis = Redis.new(:url => ENV['REDISTOGO_URL'])
    end
  end

  def add_keys_to_index(index)
    token = form_authenticity_token
    puts request_forgery_protection_token
    index.sub(/CSRF_TOKEN/, token)
  end
end