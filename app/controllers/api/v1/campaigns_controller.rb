class Api::V1::CampaignsController < ApplicationController

  def index
    # Only return the shop in the session
    shop      = Shop.find_by(shop_url: session[:shop_url])
    campaigns = Campaign.where(shop_id: shop.id)

    range     = Date.today

    puts case params[:range]
      when 'today'
        range = Date.today
      when 'yesterday'
        range = Date.today - 1
      when 'last+week'
        range = 1.week.ago
      when 'last+month'
        range = 1.month.ago
      when 'custom'
        # TODO figure out custom ranges
        range = Date.today
      else
        range = Date.today
    end

    logger.info(range)

    # We only display the range of daily stats specified by the user
    # for each campaign.
    # TODO: implement date range
    for campaign in campaigns
      dailyStats = DailyStat.where("created_at >= ? and campaign_id = ?", 
        range, campaign.id)
      campaign.views    = dailyStats.sum(:views)
      campaign.sales    = dailyStats.sum(:sales)
      campaign.spend    = dailyStats.sum(:spend)
      campaign.revenue  = dailyStats.sum(:revenue)
    end

    if campaigns
      render json: campaigns, each_serializer: CampaignSerializer
    else
      logger.error("no shop or campaign found in session")
    end
  end

  def show
    shop      = Shop.find_by(shop_url: session[:shop_url])
    campaign  = Campaign.where(shop_id: shop.id, id: params[:id])

    #show all the daily stats for this campaign
    if campaign
      render json:campaign, each_serializer: CampaignSerializer
    else
      logger.error('no shop or campaign found')
    end
  end

  def create
    shop      = Shop.find_by(shop_url: session[:shop_url])
    campaign   = Campaign.new(:name =>  params[:campaign][:name], :param =>  params[:campaign][:param],
                              :shop_id => shop.id)
    campaign.active = true
    if campaign.save
      campaigns = Campaign.where(shop_id: shop.id)
      render json: campaign, each_serializer: CampaignSerializer
    else
      logger.error("could not save")
    end
  end

  def update
    shop      = Shop.find_by(shop_url: session[:shop_url])
    campaign  = Campaign.find_by(shop_id: shop.id, id: params[:id])
    if campaign.update_attributes(:name =>  params[:campaign][:name], :param =>  params[:campaign][:param],
      :active =>  params[:campaign][:active], :spend =>  params[:campaign][:spend])
      render json: campaign, each_serializer: CampaignSerializer
    else
      #render json: { errors: campaignt.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    shop      = Shop.find_by(shop_url: session[:shop_url])
    campaign  = Campaign.find_by(shop_id: shop.id, id: params[:id])

    # Delete all daily stats attached to this campaign
    dailyStats  = DailyStat.where(campaign_id: campaign.id)
    for dailyStat in dailyStats
      dailyStat.destroy
    end

    if campaign
      campaign.destroy
      render json: campaign, each_serializer: CampaignSerializer
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    # def set_campaign
    #   @campaign = Campaign.find(params[:id])
    # end

    # Only allow a trusted parameter "white list" through.
    # def invoice_item_params
    #   params.require(:invoice_item).permit(:amount, :description, :client_id, :payment)
    # end
end
