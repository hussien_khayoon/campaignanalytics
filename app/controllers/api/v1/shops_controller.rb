class Api::V1::ShopsController < ApplicationController

  def index
    #only return the shop in the session
    shop = Shop.find_by(shop_url: session[:shop_url])

    if shop
      render json: shop, each_serializer: ShopSerializer
    else
      logger.error("no shop found in session")
    end
  end

  def show
  end
end
