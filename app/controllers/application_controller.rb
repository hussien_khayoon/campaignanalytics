class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # We skip crsf checks on cors post and options 
  skip_before_filter :verify_authenticity_token, :only => [:cors_preflight_check]

  # We always do the cors preflight check on options before post
  before_filter :cors_preflight_check
  after_filter  :cors_set_access_control_headers

  def cors_set_access_control_headers
    if request.method == 'POST'
      origin = request.headers["HTTP_ORIGIN"]
    else
      origin = '*'
    end
    headers['Access-Control-Allow-Origin'] = origin
    headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'X-ACCESS_TOKEN, Access-Control-Allow-Origin, Authorization, Origin, x-requested-with, Content-Type, Content-Range, Content-Disposition, Content-Description'
    headers['Access-Control-Allow-Credentials'] = 'true'
  end

  def cors_preflight_check
    if request.method == 'OPTIONS'
      origin = request.headers["HTTP_ORIGIN"]
      headers['Access-Control-Allow-Origin'] = origin
      headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
      headers['Access-Control-Allow-Headers'] = 'X-ACCESS_TOKEN, Access-Control-Allow-Origin, Authorization, Origin, x-requested-with, Content-Type, Content-Range, Content-Disposition, Content-Description'
      headers['Access-Control-Allow-Credentials'] = 'true'
      render :text => '', :content_type => 'text/plain'
    end
  end
end