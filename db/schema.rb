# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160720120301) do

  create_table "campaigns", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.boolean  "active"
    t.integer  "sales",      limit: 4,   default: 0
    t.float    "revenue",    limit: 24,  default: 0.0
    t.float    "spend",      limit: 24,  default: 0.0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "param",      limit: 255
    t.integer  "shop_id",    limit: 4
    t.integer  "views",      limit: 4,   default: 0
  end

  create_table "daily_stats", force: :cascade do |t|
    t.integer  "views",       limit: 4,  default: 0
    t.integer  "sales",       limit: 4,  default: 0
    t.float    "revenue",     limit: 24, default: 0.0
    t.float    "spend",       limit: 24, default: 0.0
    t.integer  "campaign_id", limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "daily_stats", ["created_at"], name: "index_daily_stats_on_created_at", using: :btree

  create_table "referral_records", force: :cascade do |t|
    t.string   "ref",                limit: 255
    t.string   "shopify_order_id",   limit: 255
    t.string   "shopify_order_json", limit: 255
    t.string   "product_name",       limit: 255
    t.float    "product_price",      limit: 24
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "shop_id",            limit: 4
    t.integer  "campaign_id",        limit: 4
  end

  create_table "shops", force: :cascade do |t|
    t.string   "shop_url",            limit: 255
    t.string   "shop_name",           limit: 255
    t.string   "shop_email",          limit: 255
    t.string   "access_token",        limit: 255
    t.string   "login_cookie",        limit: 255
    t.integer  "recurring_charge_id", limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "is_installed"
    t.integer  "shopify_shop_id",     limit: 4
  end

end
