# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

2.times do |i|
  shop = Shop.create!(
    :shop_url => 'test-shop' + i.to_s,
    :shop_name => 'testing store' + i.to_s
  )
end

2.times do |i|
  campaign = Campaign.create!(
    :name     => 'test' + i.to_s,
    :active   => true,
    :param    => 'testing' + i.to_s,
    :shop_id  => 3
  )
end