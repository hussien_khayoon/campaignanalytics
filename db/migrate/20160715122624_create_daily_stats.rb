class CreateDailyStats < ActiveRecord::Migration
  def change
    create_table :daily_stats do |t|
      t.integer :views
      t.integer :sales
      t.float :revenue
      t.float :spend
      t.integer :campaign_id

      t.timestamps null: false
    end
  end
end
