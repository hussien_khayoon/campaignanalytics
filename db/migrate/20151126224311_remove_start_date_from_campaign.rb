class RemoveStartDateFromCampaign < ActiveRecord::Migration
  def change
    remove_column :campaigns, :start_date, :datetime
  end
end
