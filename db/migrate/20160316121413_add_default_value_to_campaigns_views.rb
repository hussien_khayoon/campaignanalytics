class AddDefaultValueToCampaignsViews < ActiveRecord::Migration
  def change
  	change_column_default :campaigns, :views, 0
  end
end
