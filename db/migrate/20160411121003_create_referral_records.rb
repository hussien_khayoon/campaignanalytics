class CreateReferralRecords < ActiveRecord::Migration
  def change
    create_table :referral_records do |t|
      t.string :ref
      t.string :shopify_order_id
      t.string :shopify_order_json
      t.string :product_name
      t.float :product_price

      t.timestamps null: false
    end
  end
end
