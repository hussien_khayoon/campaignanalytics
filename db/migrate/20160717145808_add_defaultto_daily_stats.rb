class AddDefaulttoDailyStats < ActiveRecord::Migration
  def change
    change_column_default :daily_stats, :sales, 0
    change_column_default :daily_stats, :revenue, 0
    change_column_default :daily_stats, :spend, 0
    change_column_default :daily_stats, :views, 0
  end
end
