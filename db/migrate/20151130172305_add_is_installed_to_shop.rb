class AddIsInstalledToShop < ActiveRecord::Migration
  def change
    add_column :shops, :is_installed, :boolean
  end
end
