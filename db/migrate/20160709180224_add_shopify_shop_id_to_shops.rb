class AddShopifyShopIdToShops < ActiveRecord::Migration
  def change
    add_column :shops, :shopify_shop_id, :integer
  end
end
