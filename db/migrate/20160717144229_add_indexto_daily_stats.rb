class AddIndextoDailyStats < ActiveRecord::Migration

  def change
	add_index :daily_stats, :created_at
  end

end
