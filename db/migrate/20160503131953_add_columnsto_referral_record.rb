class AddColumnstoReferralRecord < ActiveRecord::Migration
  def change
  	add_column :referral_records, :shop_id, :integer
    add_column :referral_records, :campaign_id, :integer
  end
end
