class AddViewsToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :views, :integer
  end
end
