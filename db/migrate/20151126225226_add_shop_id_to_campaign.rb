class AddShopIdToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :shop_id, :integer
  end
end
