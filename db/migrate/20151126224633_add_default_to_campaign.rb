class AddDefaultToCampaign < ActiveRecord::Migration
  def up
    change_column :campaigns, :sales, :integer, :default => 0
    change_column :campaigns, :revenue, :float, :default => 0
    change_column :campaigns, :spend, :float, :default => 0
  end

  def down
    change_column :campaigns, :sales, :integer, :default => nil
    change_column :campaigns, :revenue, :float, :default => nil
    change_column :campaigns, :spend, :float, :default => nil
  end
end
