class ConvertCampaignStatstoDailyStats < ActiveRecord::Migration
  def change
  	# Go through all the shops and convert the campaigns to daily stats of today
  	shops      = Shop.all 
  	for shop in shops
  		# find all campigns belonging to this shop and then 
  		# convert them to date
  		campaigns = Campaign.where(shop_id: shop.id)
  		for campaign in campaigns
  			dailyStat 						= DailyStat.new
  			dailyStat.views 			= campaign.views
  			dailyStat.sales				= campaign.sales
  			dailyStat.revenue 		= campaign.revenue
  			dailyStat.campaign_id = campaign.id
  			dailyStat.save
  		end
  	end
  end
end
