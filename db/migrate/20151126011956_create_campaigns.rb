class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string :name
      t.boolean :active
      t.datetime :start_date
      t.integer :sales
      t.float :revenue
      t.float :spend

      t.timestamps null: false
    end
  end
end
