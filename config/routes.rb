Rails.application.routes.draw do
  # route for handling all options verb cors preflight checks
  match '*all' =>  'application#cors_preflight_check', via: [:options]
  get '/dashboard' => 'home#index'
  get '/dashboard/*other' => 'home#index'
  
  get   '/install'  => 'shops#new' 
  post  '/install'  => 'shops#create'
  get   '/login'    => 'shops#login'

  namespace :api do
    namespace :v1 do
      get :csrf,   to: 'csrf#index'
      get "/shops" =>     "shops#index"
      resources :campaigns
    end
  end

  get   '/shopify/callback', to: 'shops#finalize_install'
  get   '/shopify/orders',   to: 'shops#orders'
  get   '/shopify/campaign-orders', to: 'shops#get_orders_for_campaign'

  post  '/shopify/create_cookie', to: 'shops#create_cookie' #TODO change this endpoint to dash
  post '/shopify/webhook/app/uninstalled', to: 'shops#app_uninstall'
  post  '/shopify/webhook/orders/create',  to: 'shops#order_confirmation_event'
  match '/shopify/webhook/orders/create' => 'shop#options_orderconf', via: [:options]

  post  '/shopify/order_confirmation', to: 'shops#record_stats'
  match '/shopify/order_confirmation' => 'shop#options_orderconf', via: [:options]

end

######
# to get things working this is what I need to do
# 1) query orders from shopify
# 2) install scriptag/sales webhook
# 3) permalink tracking (track referral in cookie or shopify ref)
# 4) when sale occurs tracks sale to ref
# 5) experiment with marketing api for facebook.
# 6) style app to look acceptable for a demo